//-------------------------------------------------------
// Pull-down and Pull-up callbacks for "Short Pull" page
//-------------------------------------------------------

(
function shortPullPagePullImplementation($) { 
 
      
  function gotPullDownData(event, data) {		
    data.iscrollview.refresh(); 
  }
  
  function onPullDown (event, data) { 
    //~ setTimeout(function fakeRetrieveDataTimeout() { 
      //~ gotPullDownData(event, data); 
    //~ }, 1000);
    
   
   gotPullDownData (event, app.getColour (data));
  }  
  
  
  

  
  
  
  function onPullUp (event, data) { 
    //~ setTimeout(function fakeRetrieveDataTimeout() { 
      //~ gotPullUpData(event, data);   
      //~ }, 1500); 
  }   
  
  $(document).delegate("#index", "pageinit", 
    function bindShortPullPagePullCallbacks(event) {
      $(".iscroll-wrapper", this).bind( {
      iscroll_onpulldown : onPullDown,
      iscroll_onpullup   : onPullUp
      } );
	}
  ); 
 
}(jQuery));


