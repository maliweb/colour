//Globals:
var start_colour = "#49F4BE";
var newColour = "";
var deviceId = 0;
var sel_colour ="";
var primKey = 0;


var app = {
    // Application Constructor
    initialize: function() {
        if (navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry)/)) {
            document.addEventListener("deviceready", this.onDeviceReady, false);
            document.addEventListener("offline", this.onDeviceOffline, false);
            document.addEventListener("online", this.onDeviceOnline, false);
            document.addEventListener("resume", this.onAppResume, false);
        } else {
            this.onDeviceReady();
            this.onDeviceOffline();
            this.onDeviceOnline();
            this.onAppResume();
        }
    },


    onDeviceReady: function() {
        app.overrideBrowserAlert();
        //alert ("Ready!");
        app.openDb();
        // to simulate a virgin install umcomment the following:
        //app.dropTable();
		app.createTable();
        
    },


	onDeviceOffline: function() {
		alert ("Offline!");
	},
	
	
	onDeviceOnline: function() {
		alert ("Online!");
	},	
	
		
	onAppResume: function() {
		//alert ("Resume!");
	},
	
		
    overrideBrowserAlert: function() {
        if (navigator.notification) { // Override default HTML alert with native dialog
            window.alert = function (message) {
                navigator.notification.alert(
                    message,    // message
                    null,       // callback
                    "Toptal", // title
                    'OK'        // buttonName
                );
            };
        }
    },


	changePage: function() {
		setTimeout(function(){
			//alert ("second ?");
			$.mobile.navigate( "#second", { transition : "flip", info: "info about the #bar hash" });
		}, 1000);
	},
	
	
    
    openDb: function() {
		if (window.sqlitePlugin !== undefined) {
			app.db = window.sqlitePlugin.openDatabase("colour.db");
			//alert('openDB ok!');
		} else {
			// For debugging in simulator fallback to native SQL Lite
			app.db = window.openDatabase("colour.db", "1.0", "Cordova Demo", 200000);
		}
	},
	
	
	createTable: function() {
		app.db.transaction(function(tx) {
			tx.executeSql("CREATE TABLE colourtable (id integer primary key autoincrement, screen_colour text, device_id integer)", [],
				function(tx, r) {						
						//alert("2 Created table!");
						app.insertColourtable();
				}, app.selectColour);
		});
	},
	
	
	dropTable: function() {
		app.db.transaction(function(tx) {
			tx.executeSql("DROP TABLE IF EXISTS colourtable", []
			,alert('1 Dropping all tables!')
                        );
		});
	},
	
	
        // inserts default values into table
	insertColourtable: function() {
		//alert('Insert into colourtable!');
		app.db.transaction(function(tx) {				
				tx.executeSql("INSERT INTO colourtable (screen_colour, device_id) VALUES (?,?)",					
							[start_colour, 0],
							function(tx, r) {						
								//alert('3 Colour '+colour+' inserted!');
								app.selectColour();						
							}, app.onError);			
		});	
	},
        
        
        insertNewColour: function(colour, devid) {
		//alert('Insert into colourtable!') ;
		app.db.transaction(function(tx) {				
				tx.executeSql("UPDATE colourtable SET screen_colour='"+colour+"', device_id='"+devid+"' WHERE id="+primKey,					
							[],
							function(tx, r) {						
								//alert('6 Colour '+colour+' id '+devid+' inserted!');						
							}, app.onError);			
		});	
	},
	
	
	selectColour: function(fn) {
		app.db.transaction(function(tx) {
			tx.executeSql("SELECT * FROM colourtable", [],
						  function (tx, res) {
							  
							  sel_colour = res.rows.item(0).screen_colour;
							  deviceId = res.rows.item(0).device_id;
							  primKey = res.rows.item(0).id;
							  //alert(' selected: ' + sel_colour + ' device: '+deviceId+' Key: '+primKey);
							  app.getColour();
                                                      },
								app.onError);
		});
	},
	
	
	onError: function(e) {
		alert ('Error: '+e);
		},
		
	getColour: function (sendit) {
            var ob = {}; 
            // Hier bekomme ich den Offset der lokalen Zeitzone
            ob.offset = new Date().getTimezoneOffset();
            ob.unique=deviceId;
                //alert ('Offset: '+ob.offset);
                //sending data with a "triggervalue"-"trgVal" which is identified as POST - Variable
                
                //alert (" Send Dev: "+deviceId);
		$.ajax({ 
			type: "POST",
			url: 'http://pitens.mooo.com/boomProjekt/appconnect.php',
			async: false,
			data: {"device": JSON.stringify(ob)}, 
			datatype:"json",
			success: function(result)
			{    
								
								ob =JSON.parse(result);
                                //alert(' ' + JSON.stringify(ob));
				//alert("Remote colour : "+ob.colourdata[2].DATA + "device-db: " + sel_colour+' ret id: '+ob.colourdata[0].DATA);
                                
                                var newColour = ob.colourdata[2].DATA;
                               
                                // check if i have got a new Device ID
                                
                                
                                if ((newColour != sel_colour && newColour != "") || deviceId == 0) {
                                    if (deviceId == 0){
                                        // could be only the device id changes and not the colour
                                        deviceId = ob.colourdata[0].DATA;
                                        //alert ('newId: '+deviceId);
                                    }
                                    sel_colour = newColour;
                                    //alert ("if newC: "+newColour);
                                    $('.iscroll-scroller').css('background', newColour);
                                    
                                    //alert("after devid: "+deviceId);
                                    app.insertNewColour(newColour, deviceId);                                   
                                } else {
                                    $('.iscroll-scroller').css('background', sel_colour);
                                }                            
                                //alert ("nr.of users online: "+ob.colourdata[1].DATA); 
                                                                              
                                $('.iscroll-scroller').append('<p>Today already viewed by:<br>'+ob.colourdata[1].DATA+'</p>');
                                app.setColor(sel_colour); 
                                
                                $('p').fadeOut(3000, function(){                                 
                                });
                                //~ function sleep(milliseconds) {
								  //~ var start = new Date().getTime();
								  //~ for (var i = 0; i < 1e7; i++) {
									//~ if ((new Date().getTime() - start) > milliseconds){
									  //~ break;
									//~ }
								  //~ }
								//~ }
								//~ sleep(1000);
								
			},
			complete:function remove(){ 
			},
			error: function(){
				//alert('Cannot Login to remote DB!');
                $('.iscroll-scroller').css('background', sel_colour);
                $( "#popupDialog1" ).popup( "open" ); 
                setTimeout(function() {
                    $( "#popupDialog1" ).popup( "close" );
                }, 1000); 
                return 1;            
			}
		});	
		return sendit;
	},
	
	
	
	
	
	
	// adjust font colour to black/white according to contrast functions
	// intial function. Gets a value in the format #123456
	setColor: function (color) {
		var unpack = app.unpack(color);
		  app.color = color;
		  //alert(app.color);
		  app.rgb = unpack;
		  app.hsl = app.RGBToHSL(app.rgb);
		  app.updateDisplay();
		//return this;
	},
  
  
  
  
	  unpack: function (color) {
		if (color.length == 7) {
		  return [parseInt('0x' + color.substring(1, 3)) / 255,
			parseInt('0x' + color.substring(3, 5)) / 255,
			parseInt('0x' + color.substring(5, 7)) / 255];
		}
		else if (color.length == 4) {
		  return [parseInt('0x' + color.substring(1, 2)) / 15,
			parseInt('0x' + color.substring(2, 3)) / 15,
			parseInt('0x' + color.substring(3, 4)) / 15];
		}
	  },



	  RGBToHSL: function (rgb) {
		var min, max, delta, h, s, l;
		var r = rgb[0], g = rgb[1], b = rgb[2];
		min = Math.min(r, Math.min(g, b));
		max = Math.max(r, Math.max(g, b));
		delta = max - min;
		l = (min + max) / 2;
		s = 0;
		if (l > 0 && l < 1) {
		  s = delta / (l < 0.5 ? (2 * l) : (2 - 2 * l));
		}
		h = 0;
		if (delta > 0) {
		  if (max == r && max != g) h += (g - b) / delta;
		  if (max == g && max != b) h += (2 + (b - r) / delta);
		  if (max == b && max != r) h += (4 + (r - g) / delta);
		  h /= 6;
		}
		return [h, s, l];
	  },

// fb.updateDisplay = function () {
//   $(body).css({
//     backgroundColor: fb.color,
//     color: fb.hsl[2] > 0.5 ? '#000' : '#fff'
//   });
// }

// Set background/foreground color
	updateDisplay: function () {
	  $('p').css({
		'color': app.hsl[2] > 0.5 ? '#000' : '#fff'
	  });
	}


// $(document).ready(function() {
//   fb.setColor();
// });


	//~ fb.value = function (){
			//~ var colortext = document.getElementById("selColour").value;
			//~ alert(colortext);
			//~ fb.setColor(colortext);
		//~ }

};

app.initialize();
